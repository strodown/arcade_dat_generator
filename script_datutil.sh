#!/bin/bash

# check System
# Declare an array of string with type
declare -a Systems=("Mame" "Atomiswave" "Naomi" "NaomiGd" "Neogeo" "Model3" "STV" "Naomi2" "Triforce")

# change directory for execute datutil in buildroot
cd '../output/build/arcade-dat-generator-master/'

# check version of mame romset
Mame_Version=$(ls mame*.xml |awk -F"e" '{print $2}' |awk -F. '{print $1}')

# dat source in MameDev
# https://github.com/mamedev/mame/releases/download/mame0240/mame0240lx.zip
# need use "mameXXXX.xml" for create all dat to Recalbox
Dat_Input="mame"${Mame_Version}".xml" # -o option

# Headers dats informations
Description_Dat_NoClones=_NoClones-"${Mame_Version}" # -F option
Description_Dat=-"${Mame_Version}" # -F option
RefName=  # -R option
# Category=Arcade # -C option
# Author="Strodown, Bega48000, Chblitz62" # -A option
# Email="strodown@gmail.com" # -E option
# HomePage="Recalbox" # -H option
# Url="https://www.recalbox.com"
Comment_NoClones="NoClones Romset ${Mame_Version}" # -O option
Comment="Romset ${Mame_Version}"

# préconfiguration
Output_Format="listxml" #"genericxml" # -f option

Dat_Output_Path='Dat_pixL_Arcade' # -o option
list_Filter_Path='DatUtils-pixL'

[ "${Systems[0]}" ] ## Mame
	echo Start ${Systems[0]} dat
	echo On $Mame_Version Romset 
	echo Saved in $Dat_Output_Path path
	mkdir -p log
	## first remove all system and game type don't need in recalbox 
	## for use official mame set add -X option
	./datutil -k -X \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G @"${list_Filter_Path}"/"${Systems[0]}"/SourceFiles_Removed \
	-g @"${list_Filter_Path}"/"${Systems[0]}"/RomOf_Removed -! \
	-C "${Systems[0]} ${Category}" \
	-R "${Systems[0]}" \
	-F "${Systems[0]}${Description_Dat}" \
	-O "${Systems[0]} ${Comment}" \
	-o "${Dat_Output_Path}/${Systems[0]}-${Mame_Version}.dat" "${Dat_Input}" \
	> log/${Systems[0]}.log
	echo "MAME DAT ARCADE --- OK "
	# and remove all clones
	./datutil -k -X \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G @"${list_Filter_Path}"/"${Systems[0]}"/SourceFiles_Removed \
	-g @"${list_Filter_Path}"/"${Systems[0]}"/RomOf_Removed -! -r \
	-C "${Systems[0]} ${Category}" \
	-R "${Systems[0]}" \
	-F "${Systems[0]}${Description_Dat_NoClones}" \
	-O "${Systems[0]} ${Comment_NoClones}" \
	-o "${Dat_Output_Path}/${Systems[0]}-${Mame_Version}_NoClones.dat" "${Dat_Input}" \
	> log/${Systems[0]}_NoClones.log
	echo "MAME DAT ARCADE NO CLONES --- OK"
	# # add fr clones
	# ./datutil -k -X \
	# -V "${Dat_Number_Version}" \
	# -f "${Output_Format}" \
	# -G @"${list_Filter_Path}"/"${Systems[0]}"/SourceFiles_Removed \
	# -g @"${list_Filter_Path}"/"${Systems[0]}"/RomOf_Removed -! -r \
	# -g @"${list_Filter_Path}"/"${Systems[0]}"/RomOf_FrClones \
	# -C "${Systems[0]} ${Category}" \
	# -R "${Systems[0]}" \
	# -F "${Systems[0]}${Description_Dat_NoClones}" \
	# -O "${Systems[0]} ${Comment_NoClones}" \
	# -o "${Dat_Output_Path}/${Systems[0]}-${Mame_Version}_FrClones.dat" "${Dat_Input}" \
	# > log/${Systems[0]}_NoClones.log
	# echo "MAME DAT ARCADE FR CLONES --- OK"
[ "${Systems[1]}" ] ## Atomiswave
	echo Start ${Systems[1]} dat
	echo On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G sega/dc_atomiswave.cpp \
	-C "${Systems[1]} ${Category}" \
	-R "${Systems[1]}" \
	-F "${Systems[1]}${Description_Dat}" \
	-O "${Systems[1]} ${Comment}" \
	-o "${Dat_Output_Path}/${Systems[1]}-${Mame_Version}.dat" "${Dat_Input}" \
	> log/${Systems[1]}.log
	echo "ATOMISWAVE DAT ARCADE --- OK"
	## directly create without clones with "RomOf_Selected" list of roms
	# 	-g @"${list_Filter_Path}"/"${Systems[1]}"/RomOf_Selected \
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -G sega/dc_atomiswave.cpp \
	-C "${Systems[1]} ${Category}" \
	-R "${Systems[1]}" \
	-F "${Systems[1]}${Description_Dat_NoClones}" \
	-O "${Systems[1]} ${Comment_NoClones}" \
	-o "${Dat_Output_Path}/${Systems[1]}-${Mame_Version}_NoClones.dat" "${Dat_Input}" \
	> log/${Systems[1]}.log
	echo "ATOMISWAVE DAT ARCADE NO CLONES --- OK"
[ "${Systems[2]}" ] ## Naomi
	echo Start ${Systems[2]} dat
	echo On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	## Only naomi.cpp driver but on this naomi, naomi2, naomigd cleaning on last stage
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G sega/naomi.cpp -f "${Output_Format}" \
	-C "${Systems[2]} ${Category}" \
	-R "${Systems[2]}" \
	-F "${Systems[2]}${Description_Dat}" \
	-O "${Systems[2]} ${Comment}" \
	-o ""${list_Filter_Path}"/${Systems[2]}/${Systems[2]}_Full-Cpp.dat" "${Dat_Input}" \
	> log/${Systems[2]}_Full_Cpp.log
	## Remove naomi2 naomigd with "RomOf_Removed"
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-g @"${list_Filter_Path}"/"${Systems[2]}"/RomOf_removed -! \
	-C "${Systems[2]} ${Category}" \
	-R "${Systems[2]}" \
	-F "${Systems[2]}${Description_Dat}" \
	-O "${Systems[2]} ${Comment}" \
	-o "${Dat_Output_Path}/${Systems[2]}-${Mame_Version}.dat" ""${list_Filter_Path}"/${Systems[2]}/${Systems[2]}_Full-Cpp.dat" \
	> log/${Systems[2]}_NoClones.log
	echo "NAOMI DAT ARCADE --- OK"
	## Remove all clones for naomi
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-g @"${list_Filter_Path}"/"${Systems[2]}"/RomOf_removed -! \
	-C "${Systems[2]} ${Category}" \
	-R "${Systems[2]}" \
	-F "${Systems[2]}${Description_Dat_NoClones}" \
	-O "${Systems[2]} ${Comment_NoClones}" \
	-r -o "${Dat_Output_Path}/${Systems[2]}-${Mame_Version}_NoClones.dat" ""${list_Filter_Path}"/${Systems[2]}/${Systems[2]}_Full-Cpp.dat" \
	> log/${Systems[2]}_NoClones.log
	echo "NAOMI DAT ARCADE NO CLONES --- OK"
[ "${Systems[3]}" ] ## Naomi GD
	echo Start ${Systems[3]} dat
	echo On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	# only naomi gd no clones with "RomOf_Selected"
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -g @"${list_Filter_Path}"/"${Systems[3]}"/RomOf_Selected \
	-C "${Systems[3]} ${Category}" \
	-R "${Systems[3]}" \
	-F "${Systems[3]}${Description_Dat_NoClones}" \
	-O "${Systems[3]} ${Comment_NoClones}" \
	-o "${Dat_Output_Path}/${Systems[3]}-${Mame_Version}_NoClones.dat" "${Dat_Input}" \
	> log/${Systems[3]}_NoClones.log

	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-g @"${list_Filter_Path}"/"${Systems[3]}"/RomOf_Selected \
	-C "${Systems[3]} ${Category}" \
	-R "${Systems[3]}" \
	-F "${Systems[3]}${Description_Dat}" \
	-O "${Systems[3]} ${Comment}" \
	-o "${Dat_Output_Path}/${Systems[3]}-${Mame_Version}.dat" ""${list_Filter_Path}"/${Systems[2]}/${Systems[2]}_Full-Cpp.dat" \
	> log/${Systems[3]}_NoClones.log

	echo "NAOMIGD DAT ARCADE NO CLONES --- Ok"
[ "${Systems[4]}" ] ## Neogeo
	echo Start ${Systems[4]} dat
	echo On $Mame_Version Romset 
	echo Saved in $Dat_Output_Path path
	## select only neogeo.cpp driver with clones
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G neogeo/neogeo.cpp \
	-C "${Systems[4]} ${Category}" \
	-R "${Systems[4]}" \
	-F "${Systems[4]}${Description_Dat}" \
	-V "${Dat_Number_Version}" \
	-O "${Systems[4]} ${Comment_Clones}" \
	-o ""${list_Filter_Path}"/${Systems[4]}/${Systems[4]}_Full-Cpp.dat" "${Dat_Input}" > log/${Systems[4]}_Full_Cpp.log && cp ""${list_Filter_Path}"/${Systems[4]}/${Systems[4]}_Full-Cpp.dat" "${Dat_Output_Path}/${Systems[4]}-${Mame_Version}.dat" 
	## remove all clone in neogeo and other not a games
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -g @"${list_Filter_Path}"/"${Systems[4]}"/RomOf_Removed -! \
	-C "${Systems[4]} ${Category}" \
	-R "${Systems[4]}" \
	-F "${Systems[4]}${Description_Dat_NoClones}" \
	-O "${Systems[4]} ${Comment}" \
	-o "${Dat_Output_Path}/${Systems[4]}-${Mame_Version}_NoClones.dat" ""${list_Filter_Path}"/${Systems[4]}/${Systems[4]}_Full-Cpp.dat" \
	> log/${Systems[4]}_NoClones.log
	echo "NEOGEO DAT ARCADE NO CLONES --- OK"
[ "${Systems[5]}" ] ## model3
	echo Start ${Systems[5]} dat
	echo On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	## Only model3.cpp driver
	# -M option for merging (none, split or full)
	# 	-p "device_ref" \
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-M "none" -G sega/model3.cpp \
	-C "${Systems[5]} ${Category}" \
	-R "${Systems[5]}" \
	-F "${Systems[5]}${Description_Dat}" \
	-O "${Systems[5]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[5]}-${Mame_Version}.dat" "${Dat_Input}" \
	> log/${Systems[5]}.log
	echo "MODEL3 DAT ARCADE --- OK"
	## Patch Mame romset to Supermodel need to add segabill and more eeprom for working games
	patch "${Dat_Output_Path}/${Systems[5]}-${Mame_Version}.dat" ""${list_Filter_Path}"/${Systems[5]}/Mame_to_Model3.patch" 
	## Only model3.cpp No Clones
	#	-p "device_ref" \
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -M "none" -G sega/model3.cpp \
	-C "${Systems[5]} ${Category}" \
	-R "${Systems[5]}" \
	-F "${Systems[5]}${Description_Dat_NoClones}" \
	-V "${Dat_Number_Version}" \
	-O "${Systems[5]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[5]}-${Mame_Version}_NoClones.dat" "${Dat_Input}" \
	> log/${Systems[5]}.log
	echo "MODEL3 DAT ARCADE NO CLONES --- OK"
	## Patch Mame romset to Supermodel need to add segabill and more eeprom for working games
	patch "${Dat_Output_Path}/${Systems[5]}-${Mame_Version}_NoClones.dat" ""${list_Filter_Path}"/${Systems[5]}/Mame_to_Model3_NoClones.patch" 
	echo "MODEL3 PATCH MAME ROMSET --- OK"
[ "${Systems[6]}" ] ## STV
	echo Start ${Systems[6]} dat On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	## Only STV.cpp driver 
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G sega/stv.cpp \
	-C "${Systems[6]} ${Category}" \
	-R "${Systems[6]}" \
	-F "${Systems[6]}${Description_Dat}" \
	-O "${Systems[6]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[6]}-${Mame_Version}.dat" "${Dat_Input}" \
	> log/${Systems[6]}.log
	echo "STV DAT ARCADE --- OK"
	## Only STV.cpp No Clones
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -G sega/stv.cpp \
	-C "${Systems[6]} ${Category}" \
	-R "${Systems[6]}" \
	-F "${Systems[6]}${Description_Dat_NoClones}" \
	-O "${Systems[6]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[6]}-${Mame_Version}_NoClones.dat" "${Dat_Input}" \
	> log/${Systems[6]}.log
	echo "STV DAT ARCADE NO CLONES --- OK"
[ "${Systems[7]}" ] ## Naomi2
	echo Start ${Systems[7]} dat
	echo On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	## Only naomi2.cpp driver
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-g @"${list_Filter_Path}"/"${Systems[7]}"/RomOf_removed -! \
	-C "${Systems[7]} ${Category}" \
	-R "${Systems[7]}" \
	-F "${Systems[7]}${Description_Dat}" \
	-O "${Systems[7]} ${Comment}" \
	-o "${Dat_Output_Path}/${Systems[7]}-${Mame_Version}.dat" ""${list_Filter_Path}"/${Systems[2]}/${Systems[2]}_Full-Cpp.dat" \
	> log/${Systems[7]}_Full_Cpp.log
	echo "Naomi2 DAT ARCADE --- OK"
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -g @"${list_Filter_Path}"/"${Systems[7]}"/RomOf_removed -! \
	-C "${Systems[7]} ${Category}" \
	-R "${Systems[7]}" \
	-F "${Systems[7]}${Description_Dat_NoClones}" \
	-V "${Dat_Number_Version}" \
	-O "${Systems[7]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[7]}-${Mame_Version}_NoClones.dat" ""${list_Filter_Path}"/${Systems[2]}/${Systems[2]}_Full-Cpp.dat" \
	> log/${Systems[7]}.log
	echo "Naomi2 DAT ARCADE NO CLONES --- OK"

[ "${Systems[8]}" ] ## Triforce
	echo Start ${Systems[8]} dat On $Mame_Version Romset
	echo Saved in $Dat_Output_Path path
	## Only triforce.cpp driver 
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-G sega/triforce.cpp \
	-C "${Systems[8]} ${Category}" \
	-R "${Systems[8]}" \
	-F "${Systems[8]}${Description_Dat}" \
	-O "${Systems[8]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[8]}-${Mame_Version}.dat" "${Dat_Input}" \
	> log/${Systems[8]}.log
	echo "Triforce DAT ARCADE --- OK"
	## Only triforce.cpp No Clones
	./datutil -k \
	-V "${Dat_Number_Version}" \
	-f "${Output_Format}" \
	-r -G sega/triforce.cpp \
	-C "${Systems[8]} ${Category}" \
	-R "${Systems[8]}" \
	-F "${Systems[8]}${Description_Dat_NoClones}" \
	-O "${Systems[8]} ${Comment_Clones}" \
	-o "${Dat_Output_Path}/${Systems[8]}-${Mame_Version}_NoClones.dat" "${Dat_Input}" \
	> log/${Systems[8]}.log
	echo "Triforce DAT ARCADE NO CLONES --- OK"
exit
